// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:markets_app/popular.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:flutter/services.dart';

class Data {
  String name;
  String subname;
  String growth;
  String price;

  Data({this.name, this.subname, this.growth, this.price});
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => new _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    final mHeaderList = [
      'Trending',
      ' Favorites',
      ' Signals',
      'Top Rising',
      'Top Falling',
      'Top Volatility(1 Day)',
    ];
    final mHeaderIcon = [
      null,
      Icons.star_border,
      Icons.vibration,
      null,
      null,
      null
    ];
    final mSubName = [
      'Dow Jones',
      'Dow Jones',
      'EUR/USD',
      'USD/JPY',
      'NASDAQ 100',
      'GBP/USD',
      'USD/CAD',
      'GOLD',
      'Dow Jones',
      'EUR/USD',
      'USD/JPY',
      'NASDAQ 100',
      'GBP/USD',
    ];
    final mPrice = [
      '27 447',
      '27 447',
      '1.11373',
      '108.811',
      '8 239.88',
      '1.28988',
      '1.31317',
      '1 505.94',
      '1 505.94',
      '27 447',
      '1.11373',
      '108.811',
      '8 239.88',
    ];
    final mGrowth = [
      '+0.18%',
      '+0.18%',
      '+0.08%',
      '+0.18%',
      '+0.31%',
      '+0.15%',
      '-0.16%',
      '-0.18%',
      '-0.18%',
      '+0.31%',
      '+0.15%',
      '-0.16%',
      '-0.18%'
    ];
    final mName = [
      'YM',
      'YM',
      'EURUSD',
      'USDJPY',
      'NQ',
      'GBPUSD',
      'USDCAD',
      'XAUUSD',
      'XAUUSD',
      'NQ',
      'GBPUSD',
      'USDCAD',
      'XAUUSD',
    ];
    SystemChrome.setEnabledSystemUIOverlays([]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        primaryColor: const Color(0xFF3a3b3d),
        accentColor: const Color(0xFF7885AE),

        // Define the default font family.
        fontFamily: 'Montserrat',

        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
          title: TextStyle(fontSize: 18.0, fontStyle: FontStyle.italic),
          body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          backgroundColor: Colors.black,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(100.0), // here the desired height
            child: AppBar(
              bottom: TabBar(
                labelStyle: TextStyle(fontSize: 11),
                tabs: [
                  Tab(text: 'POPULAR'),
                  new Tab(
                    child: Row(
                      children: [
                        Icon(Icons.graphic_eq, size: 13.0),
                        new Padding(
                          padding: EdgeInsets.fromLTRB(0, 2.0, 0, 0),
                          child: Text(
                            " INDICES",
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Tab(text: 'ALL'),
                  new Tab(
                    child: Row(
                      children: [
                        Icon(Icons.live_tv, size: 15.0),
                        new Padding(
                          padding: EdgeInsets.fromLTRB(0, 3.0, 0, 0),
                          child: Text(
                            " NEWS",
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              title: new Padding(
                padding: EdgeInsets.fromLTRB(50, 0.0, 0, 0),
                child: new Row(children: <Widget>[
                Expanded(
                  child: Center(
                      child: new Text('Markets',
                          style: new TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center)),
                flex: 2,),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(width: 1.0),
                      borderRadius: BorderRadius.all(Radius.circular(
                              5.0) //                 <--- border radius here
                          ),
                    ),
                    child: new Center(
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
                            child: Text(
                              "€ 50 000.00",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 10.0,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              color: const Color(0xFF919294),
                            ),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(3, 2, 3, 3),
                              child: Text(
                                "DEMO ACCOUNT",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 8.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ]),
              ),
            ),
          ),
          body: TabBarView(
            children: [
              ListView.builder(
                  itemCount: 1,
                  itemBuilder: (context, index) {
                    return StickyHeader(
                      header: Container(
                        height: 50.0,
                        padding: EdgeInsets.all(8.0),
                        alignment: Alignment.centerLeft,
                        child: ListView(
                          // This next line does the trick.
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            for (int i = 0; i < mHeaderList.length; i++)
                              new Container(
                                margin: const EdgeInsets.all(3.0),
                                decoration: BoxDecoration(
                                  color: const Color(0xFF242527),
                                  border: i == 0
                                      ? Border.all(
                                          width: 1.0,
                                          color: const Color(0xFF7885AE))
                                      : Border.all(
                                          width: 1.0,
                                          color: const Color(0xFF3a3b3d)),
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(
                                          5.0) //                 <--- border radius here
                                      ),
                                ),
                                child: new Center(
                                  child: new Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(8, 6, 8, 6),
                                    child: Row(
                                      children: [
                                        mHeaderIcon[i] != null
                                            ? Icon(mHeaderIcon[i],
                                                size: 10.0,
                                                color: const Color(0xFF919294))
                                            : new Row(),
                                        Text(
                                          mHeaderList[i].toUpperCase(),
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontSize: 10.0,
                                            color: i != 0
                                                ? const Color(0xFF919294)
                                                : Colors.white,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                      content: new Padding(
                        padding: new EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            for (int i = 0; i < mName.length; i++)
                              Container(
                                  child: i == 0
                                      ? new Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              8.0, 0, 8.0, 8.0),
                                          child: new Row(children: [
                                            Expanded(
                                              child: Container(
                                                child: Text(
                                                  'Popular instruments',
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 10.0,
                                                    color:
                                                        const Color(0xFF919294),
                                                  ),
                                                ),
                                              ),
                                              flex: 2,
                                            ),
                                            Expanded(
                                              child: Container(
                                                child: Text(
                                                  'Price',
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 10.0,
                                                    color:
                                                        const Color(0xFF919294),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: Container(
                                                child: Text(
                                                  'Daily charges',
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 10.0,
                                                    color:
                                                        const Color(0xFF919294),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]))
                                      : new Card(
                                          color: const Color(0xFF242527),
                                          child: InkWell(
                                            onTap: () {
                                                var route = new MaterialPageRoute(
                                                  builder: (BuildContext context) =>
                                                  new PopularScreen( data: Data(
                                                      name:
                                                      mName[i],
                                                      subname:
                                                      mSubName[
                                                      i],
                                                      growth:
                                                      mGrowth[
                                                      i],
                                                      price: mPrice[
                                                      i])),
                                                );
                                                Navigator.of(context).push(route);
                                            },
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          mName[i],
                                                          textAlign:
                                                              TextAlign.left,
                                                          style: TextStyle(
                                                            fontSize: 16.0,
                                                          ),
                                                        ),
                                                        Text(mSubName[i],
                                                            style: TextStyle(
                                                              fontSize: 10.0,
                                                              color:
                                                                  Colors.grey,
                                                            )),
                                                      ],
                                                    ),
                                                    flex: 2,
                                                  ),
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(mPrice[i],
                                                            style: TextStyle(
                                                              fontSize: 16.0,
                                                            )),
                                                      ],
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .end,
                                                      children: [
                                                        mGrowth[i]
                                                                .toString()
                                                                .contains(
                                                                    '-', 0)
                                                            ? new Text(
                                                                mGrowth[i],
                                                                style:
                                                                    TextStyle(
                                                                  fontSize:
                                                                      16.0,
                                                                  color: Colors
                                                                      .red,
                                                                ))
                                                            : new Text(
                                                                mGrowth[i],
                                                                style:
                                                                    TextStyle(
                                                                  fontSize:
                                                                      16.0,
                                                                  color: Colors
                                                                      .green,
                                                                )),
                                                      ],
                                                    ),
                                                  ),
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      Icon(
                                                          Icons
                                                              .keyboard_arrow_right,
                                                          color: Colors.white30)
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        )),
                          ],
                        ),
                      ),
                    );
                  }),
              ListView.builder(
                itemCount: 0,
                scrollDirection: Axis.vertical,
                controller: ScrollController(),
                itemBuilder: (BuildContext context, int index) {
                  //if (index < 50)
                  return Container(
                    padding: EdgeInsets.all(20.0),
                    child: Center(
                      child: ListTile(
                        leading: const Icon(Icons.account_circle,
                            size: 40.0, color: Colors.white30),
                        title: Text('MainItem $index'),
                        subtitle: Text('SubText $index'),
                      ),
                    ),
                    color: Colors.blue[400],
                    margin: EdgeInsets.all(1.0),
                  );
                },
              ),
              ListView.builder(
                itemCount: 0,
                scrollDirection: Axis.vertical,
                controller: ScrollController(),
                itemBuilder: (BuildContext context, int index) {
                  //if (index < 50)
                  return Container(
                    padding: EdgeInsets.all(20.0),
                    child: Center(
                      child: ListTile(
                        leading: const Icon(Icons.account_circle,
                            size: 40.0, color: Colors.white30),
                        title: Text('MainItem $index'),
                        subtitle: Text('SubText $index'),
                      ),
                    ),
                    color: Colors.blue[400],
                    margin: EdgeInsets.all(1.0),
                  );
                },
              ),
              ListView.separated(
                itemCount: 0,
                separatorBuilder: (BuildContext context, int index) =>
                    Container(
                  padding: EdgeInsets.all(20.0),
                  child: Center(
                    child: ListTile(
                      leading: const Icon(Icons.account_circle,
                          size: 40.0, color: Colors.grey),
                      title: Text('SeperatorItem $index'),
                      subtitle: Text('SeperatorSubText $index'),
                    ),
                  ),
                  color: Colors.amber[100],
                  margin: EdgeInsets.all(1.0),
                ),
                itemBuilder: (BuildContext context, int index) {
                  //if (index < 50)
                  return Container(
                    padding: EdgeInsets.all(20.0),
                    child: Center(
                      child: ListTile(
                        leading: const Icon(Icons.account_circle,
                            size: 40.0, color: Colors.white30),
                        title: Text('MainItem $index'),
                        subtitle: Text('SubText $index'),
                      ),
                    ),
                    color: Colors.blue[400],
                    margin: EdgeInsets.all(1.0),
                  );
                },
              ),
            ],
          ),
          bottomNavigationBar: new Theme(
            data: Theme.of(context).copyWith(
                // sets the background color of the `BottomNavigationBar`
                canvasColor: Colors.black,
                // sets the active color of the `BottomNavigationBar` if `Brightness` is light
                primaryColor: const Color(0xFF7885AE),
                textTheme: Theme.of(context).textTheme.copyWith(
                    caption: new TextStyle(color: const Color(0xFF8B8B8B)))),
            child: new BottomNavigationBar(
              currentIndex: 0,
              type: BottomNavigationBarType.fixed,
              items: [
                BottomNavigationBarItem(
                    icon: Icon(Icons.trending_up), title: new Text('Markets')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.swap_vert), title: new Text('Trades')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.email), title: new Text('Inbox')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.account_balance_wallet),
                    title: new Text('Wallet')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.more_horiz), title: new Text('More'))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
