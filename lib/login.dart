import 'package:flutter/material.dart';
import 'package:markets_app/home.dart';

void main() => runApp(new LoginScreen());

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme:
          new ThemeData(primarySwatch: Colors.green, hintColor: Colors.white30),
      home: new LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

// Used for controlling whether the user is loggin or creating an account
enum FormType { login, register }

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _validate = false;
  String _email = "";
  String _password = "";
  FormType _form = FormType
      .login; // our default setting is to login, and we should switch to creating an account when the user chooses to

  // Swap in between our two forms, registering and logging in
  void _formChange() async {
    setState(() {
      if (_form == FormType.register) {
        _form = FormType.login;
      } else {
        _form = FormType.register;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: const Color(0xFF3A3B3D),
      body: new Form(
        key: _formKey,
        autovalidate: _validate,
        child: new Container(
          padding: EdgeInsets.all(16.0),
          child: new Column(
            children: <Widget>[
              Container(
                child: Expanded(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'images/ic_launcher.png',
                        height: 60,
                        width: 60,
                      ),
                      new Padding(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 0.0),
                        child: new Text(
                          "ParamountDAXasmd",
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 22.0,
                              color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                  flex: 2,
                ),
              ),
              _buildTextFields(),
              _buildButtons(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBar(BuildContext context) {}

  Widget _buildTextFields() {
    return new Container(
      child: new Column(
        children: <Widget>[
          new Container(
            child: new TextFormField(
              keyboardType: TextInputType.emailAddress,
              style: new TextStyle(color: const Color(0xFF919294)),
              decoration: new InputDecoration(
                focusColor: Colors.white,
                labelText: 'Email',
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: const Color(0xFF919294)),
                ),
              ),
              validator: validateEmail,
              onSaved: (String val) {
                _email = val;
              },
            ),
          ),
          new Container(
            child: new TextFormField(
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                    labelText: 'Password',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: const Color(0xFF919294)),
                    )),
                obscureText: true,
                validator: validatePassword,
                onSaved: (String val) {
                  _password = val;
                }),
          )
        ],
      ),
    );
  }

  Widget _buildButtons() {
    if (_form == FormType.login) {
      return new Container(
        child: new Column(
          children: <Widget>[
            new Padding(
              padding: EdgeInsets.fromLTRB(0, 16, 0, 0),
              child: SizedBox(
                width: double.infinity,
                child: new RaisedButton(
                  color: const Color(0xFF66cc99),
                  child: new Text(
                    'Login',
                    style: new TextStyle(fontSize: 15.0, color: Colors.white),
                  ),
                  onPressed: _loginPressed,
                ),
              ),
            ),
            new FlatButton(
              child: new Text(
                'Dont have an account? Tap here to register.',
                style: new TextStyle(
                    fontSize: 15.0, color: const Color(0xFF919294)),
              ),
              onPressed: _formChange,
            ),
            new FlatButton(
              child: new Text(
                'Forgot Password?',
                style: new TextStyle(
                    fontSize: 15.0, color: const Color(0xFF919294)),
              ),
              onPressed: _passwordReset,
            )
          ],
        ),
      );
    } else {
      return new Container(
        child: new Column(
          children: <Widget>[
            new Padding(
              padding: EdgeInsets.fromLTRB(0, 16, 0, 0),
              child: new SizedBox(
                width: double.infinity,
                child: new RaisedButton(
                  color: const Color(0xFF66cc99),
                  child: new Text(
                    'Create an Account',
                    style: new TextStyle(fontSize: 15.0, color: Colors.white),
                  ),
                  onPressed: _createAccountPressed,
                ),
              ),
            ),
            new FlatButton(
              child: new Text(
                'Have an account? Click here to login.',
                style: new TextStyle(
                    fontSize: 15.0, color: const Color(0xFF919294)),
              ),
              onPressed: _formChange,
            )
          ],
        ),
      );
    }
  }

  // These functions can self contain any user auth logic required, they all have access to _email and _password

  void _loginPressed() {
    if (_formKey.currentState.validate()) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }

    //print('The user wants to login with $_email and $_password');
  }

  void _createAccountPressed() {
    if (_formKey.currentState.validate()) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  void _passwordReset() {
    print("The user wants a password reset request sent to $_email");
  }

  String validatePassword(String value) {
    String patttern = r'(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$)';
    RegExp regExp = new RegExp(patttern);
    if (value.isEmpty) {
      return "Password is Required";
    } else if (value.length < 8) {
      return "Password must minimum eight characters";
    } else if (!regExp.hasMatch(value)) {
      return "Password at least one uppercase letter, one lowercase letter and one number";
    }
    return null;
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }
}
/*
import 'package:flutter/material.dart';
import 'package:markets_app/home.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreen createState() => new _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  Widget build(BuildContext context) {
    final emailField = TextField(
      obscureText: false,
      style: style,
      decoration: InputDecoration(
          enabledBorder: const OutlineInputBorder(
            // width: 0.0 produces a thin "hairline" border
            borderSide: const BorderSide(color: Colors.white, width: 0.0),
          ),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Email",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
      ),
    );
    final passwordField = TextField(
      obscureText: true,
      style: style,
      decoration: InputDecoration(
          enabledBorder: const OutlineInputBorder(
            // width: 0.0 produces a thin "hairline" border
            borderSide: const BorderSide(color: Colors.white, width: 0.0),
          ),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => HomeScreen()));
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
      body: Center(
        child: Container(
          color: const Color(0xFF3A3B3D),
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 155.0,
                  child: Image.asset(
                    "assets/ic_launcher.png",
                    fit: BoxFit.contain,
                  ),
                ),
                SizedBox(height: 45.0),
                emailField,
                SizedBox(height: 25.0),
                passwordField,
                SizedBox(
                  height: 35.0,
                ),
                loginButon,
                SizedBox(
                  height: 15.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
*/
