import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_candlesticks/flutter_candlesticks.dart';
import 'package:markets_app/home.dart';
import 'dart:async';

class PopularScreen extends StatefulWidget {
  final Data data;
  PopularScreen({Key key, this.data}) : super(key: key);

  @override
  Popular createState() => new Popular();
}

class Popular extends State<PopularScreen> {
  int number = 0;

  void subtractNumbers() {}

  void addNumbers() {}

  @override
  Widget build(BuildContext context) {
    List sampleData = [
      {
        "open": 50.0,
        "high": 100.0,
        "low": 40.0,
        "close": 80,
        "volumeto": 5000.0
      },
      {
        "open": 80.0,
        "high": 90.0,
        "low": 55.0,
        "close": 65,
        "volumeto": 4000.0
      },
      {
        "open": 65.0,
        "high": 120.0,
        "low": 60.0,
        "close": 90,
        "volumeto": 7000.0
      },
      {
        "open": 90.0,
        "high": 95.0,
        "low": 85.0,
        "close": 80,
        "volumeto": 2000.0
      },
      {
        "open": 80.0,
        "high": 85.0,
        "low": 40.0,
        "close": 50,
        "volumeto": 3000.0
      },
    ];
    return MaterialApp(
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        primaryColor: const Color(0xFF3A3B3D),
        accentColor: const Color(0xFF7885AE),

        // Define the default font family.
        fontFamily: 'Montserrat',

        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
          title: TextStyle(fontSize: 18.0, fontStyle: FontStyle.italic),
          body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: const Color(0xFF242527),
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(100.0), // here the desired height
            child: AppBar(
              bottom: TabBar(
                labelStyle: TextStyle(fontSize: 11),
                tabs: [
                  Tab(text: 'CHART'),
                  Tab(text: 'NEWS'),
                  new Tab(
                    child: Row(
                      children: [
                        Icon(Icons.person, size: 13.0),
                        new Padding(
                          padding: EdgeInsets.fromLTRB(0, 2.0, 0, 0),
                          child: Text(
                            " LIVE TRADES",
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              title: new Container(
                child: new Row(
                  children: <Widget>[
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      GestureDetector(
                        onTap:() => Navigator.of(context).pop(),
                        child: Container(
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                          child: Icon(Icons.keyboard_arrow_left,
                              size: 20, color: const Color(0xFF7885AE)),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(8, 10, 0, 10),
                        child: Text(
                          'POPULAR',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 15.0,
                            color: const Color(0xFF7885AE),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(60, 10, 0, 10),
                        child: Text(
                          '${widget.data.name}',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 13.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(3, 10, 0, 10),
                        child: Icon(Icons.star_border,
                            size: 18, color: const Color(0xFF919294)),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(70, 10, 0, 10.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(width: 1.0),
                          borderRadius: BorderRadius.all(Radius.circular(
                                  5.0) //                 <--- border radius here
                              ),
                        ),
                        child: new Center(
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
                                child: Text(
                                  "€ 0.00",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 10.0,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: const Color(0xFF919294),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(3, 2, 3, 3),
                                  child: Text(
                                    "Live Account",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 8.0,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ]),
                  ],
                ),
              ),
            ),
          ),
          body: TabBarView(
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(0, 5.0, 0, 0.0),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        color: Colors.black,
                        height: 300.0,
                        child: new OHLCVGraph(
                            data: sampleData,
                            enableGridLines: true,
                            volumeProp: 0.0,
                            gridLineAmount: 5,
                            gridLineColor: Colors.grey[300],
                            gridLineLabelColor: Colors.grey),
                      ),
                      flex: 2,
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: Column(children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${widget.data.name}',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 18.0,
                                  ),
                                ),
                                Text('${widget.data.subname}',
                                    style: TextStyle(
                                      fontSize: 10.0,
                                      color: Colors.grey,
                                    )),
                              ],
                            ),
                            Column(
                              children: [
                                Text('${widget.data.price}',
                                    style: TextStyle(
                                      fontSize: 18.0,
                                    )),
                                Text('Current price',
                                    style: TextStyle(
                                      fontSize: 10.0,
                                      color: Colors.grey,
                                    )),
                              ],
                            ),
                            Column(
                              children: [
                                Text('${widget.data.growth}',
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      color: Colors.green,
                                    )),
                                Text('Daily change',
                                    style: TextStyle(
                                      fontSize: 10.0,
                                      color: Colors.grey,
                                    )),
                              ],
                            ),
                          ],
                        ),
                        Row(
                            // ROW 3
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: Text(
                                  'Low: ',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: Text(
                                  '27 367     ',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: Text(
                                  'High: ',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                /*color: Colors.purple,
                            margin: EdgeInsets.all(25.0),*/
                                child: Text(
                                  '27 468',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ]),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new RaisedButton(
                              padding: const EdgeInsets.fromLTRB(55, 8, 55, 8),
                              textColor: Colors.white,
                              color: Colors.blue,
                              onPressed: addNumbers,
                              child: Row(
                                children: [
                                  Icon(Icons.call_received),
                                  Text('Sell'),
                                ],
                              ),
                            ),
                            new RaisedButton(
                              textColor: Colors.white,
                              color: Colors.red,
                              onPressed: addNumbers,
                              padding: const EdgeInsets.fromLTRB(55, 8, 55, 8),
                              child: Row(
                                children: [
                                  Icon(Icons.call_made),
                                  Text('Buy'),
                                ],
                              ),
                            ),
                          ],
                        )
                      ]),
                      alignment: Alignment(-1.0, -1.0),
                    ),
                  ],
                ),
              ),
              ListView.builder(
                itemCount: 0,
                scrollDirection: Axis.vertical,
                controller: ScrollController(),
                itemBuilder: (BuildContext context, int index) {
                  //if (index < 50)
                  return Container(
                    padding: EdgeInsets.all(20.0),
                    child: Center(
                      child: ListTile(
                        leading: const Icon(Icons.account_circle,
                            size: 40.0, color: Colors.white30),
                        title: Text('MainItem $index'),
                        subtitle: Text('SubText $index'),
                      ),
                    ),
                    color: Colors.blue[400],
                    margin: EdgeInsets.all(1.0),
                  );
                },
              ),
              ListView.separated(
                itemCount: 0,
                separatorBuilder: (BuildContext context, int index) =>
                    Container(
                  padding: EdgeInsets.all(20.0),
                  child: Center(
                    child: ListTile(
                      leading: const Icon(Icons.account_circle,
                          size: 40.0, color: Colors.grey),
                      title: Text('SeperatorItem $index'),
                      subtitle: Text('SeperatorSubText $index'),
                    ),
                  ),
                  color: Colors.amber[100],
                  margin: EdgeInsets.all(1.0),
                ),
                itemBuilder: (BuildContext context, int index) {
                  //if (index < 50)
                  return Container(
                    padding: EdgeInsets.all(20.0),
                    child: Center(
                      child: ListTile(
                        leading: const Icon(Icons.account_circle,
                            size: 40.0, color: Colors.white30),
                        title: Text('MainItem $index'),
                        subtitle: Text('SubText $index'),
                      ),
                    ),
                    color: Colors.blue[400],
                    margin: EdgeInsets.all(1.0),
                  );
                },
              ),
            ],
          ),
          bottomNavigationBar: new Theme(
            data: Theme.of(context).copyWith(
                // sets the background color of the `BottomNavigationBar`
                canvasColor: Colors.black,
                // sets the active color of the `BottomNavigationBar` if `Brightness` is light
                primaryColor: const Color(0xFF7885AE),
                textTheme: Theme.of(context).textTheme.copyWith(
                    caption: new TextStyle(color: const Color(0xFF8B8B8B)))),
            child: new BottomNavigationBar(
              currentIndex: 0,
              type: BottomNavigationBarType.fixed,
              items: [
                BottomNavigationBarItem(
                    icon: Icon(Icons.trending_up), title: new Text('Markets')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.swap_vert), title: new Text('Trades')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.email), title: new Text('Inbox')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.account_balance_wallet),
                    title: new Text('Wallet')),
                BottomNavigationBarItem(
                    icon: Icon(Icons.more_horiz), title: new Text('More'))
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SalesData {
  SalesData(this.year, this.sales);

  final String year;
  final double sales;
}
