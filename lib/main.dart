import 'dart:async';

import 'package:flutter/material.dart';
import 'package:markets_app/login.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(new MaterialApp(
    home: new SplashScreen(),
    routes: <String, WidgetBuilder>{
      '/LoginScreen': (BuildContext context) => new LoginScreen()
    },
  ));
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  startTime() async {
    var _duration = new Duration(seconds: 4);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.of(context).pushReplacementNamed('/LoginScreen');
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return new Scaffold(
      backgroundColor: const Color(0xFF3A3B3D),
      body: new Center(
        child: Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'images/ic_launcher.png',
                height: 60,
                width: 60,
              ),
              new Padding(
                padding: EdgeInsets.fromLTRB(0, 15, 0, 0.0),
                child: new Text(
                  "ParamountDAXasmd",
                  style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 22.0,
                      color: Colors.white),
                ),
              ),
            ],
          ),
        ), //new Image.asset('images/ic_launcher.png'),
      ),
    );
  }
}
